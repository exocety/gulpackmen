// --------------------------------------------------------------
// Imports des dépendances
// --------------------------------------------------------------
var gulp            = require('gulp');
var conf            = require('./gulpackmen/conf/application.conf.js');
var gulpackmen         = require('@exocety/gulpackmen')(gulp, conf);

// --------------------------------------------------------------
// Taches personnalisées
// --------------------------------------------------------------

// Exemple : 
gulp.task( "start", gulp.series('serve:dev') );
// Seules les taches possédant une description apparaissent automatiquement 
// gulp.task( "start" ).description = "C'est parti pour le dev !" ;

// Pour afficher l'arborescence des taches 
// gulp --tasks

