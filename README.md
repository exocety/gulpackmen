# Pourquoi utiliser Gulpackmen ?

## Pour résoudre les problèmes suivants :
- Ne plus maintenir des outils de build différents en fonction des projets 

    -> utiliser un outil de build agnostique (php ou java)

- Eviter d'avoir toutes les taches gulp dans un seul fichier
    
    -> modulariser les taches gulp dans différents fichiers

- Ne plus éditer directement les tâches gulp

    -> l'édition d'un fichier de configuration doit couvrir tous les besoins de build existant.

- Trop de dev-dépendances dans le fichier package.json 

    -> création d'un module à part avec ses propres dépendances

    -> upgrade commun de dépendances de l'outil de build

- Gestion compliquée des numéros de révision 
    
    -> build dans un répertoire .temp et injection automatique des tag `<script>` et `<link>`.

- Ne pas connaitre les noms des taches 
    
    -> proposer un menu qui se lance à l'appel de la commande gulp
    
- Ne pas ajouter des extensions pour gérer les taches en mode serie ou parallèle
    
    -> l'utilisation de gulp.series() et gulp.parallel() devient un standard dans GULP version 4

- Améliorer les performances
    
    -> Utilisation du lazy loading de modules node
    
    -> Optimisation du CSS/JS uniquement lorsque cela est nécessaire.

# Pré-requis

Installer la dernière version de gulp-cli de manière globale : 
```
npm install -g gulp-cli
```
Cela permet de reconnaitre la commande `gulp` en ligne de commande.

Attention, il ne faut pas installer gulp en version globale car par defaut, npm installe la version 3.9.1 or gulpackmen ne fonctionne qu'avec la version 4 minimum.

# Pour commencer

## Modifier le fichier `package.json` du projet
Dans la partie devDependencies, il faut spécifier la version 4 de gulp, gulpackmen hebergé sur gitlab et si besoin, ajouter handlebars

```
{ 
 ...
  "devDependencies": {
    "gulp": "^4.0.0",
    "gulpackmen": "@exocety/gulpackmen",
    "handlebars": "^4.0.10"
  }
}
```

## Modification du fichier `gulpfile.js`

Créer un fichier `gulpfile.js` avec le contenu suivant :
```
var gulp 		= require('gulp');
var gulpackmen 		= require('gulpackmen')(gulp);
```
Executer la tache suivante :
```
gulp
```
Au premier lancement, gulpackmen va copier le répertoire gulpackmen dans le répertoire build du projet
, il contient le répertoire de travail temporaire et le répertoire des fichiers de configuration
De plus, il modifie le contenu du fichier de configuration gulpfile.js pour y ajouter l'appel 
des fichiers de configuration

Si vous saisissez à nouveau la commande `gulp`, vous devez voir apparaitre un menu dans votre console.
Attention cependant, les taches définies en standard ne vont pas fonctionner car il faut au préalable 
modifier votre fichier de configuration.


## Modification du fichier de configuration du build `application.conf.js`

### Définir les chemins utilisés lors du build

Vous devez redéfinir l'objet `oPaths` pour prendre en compte tous les chemins utilisés par la procédure de build.

On peut désactiver certaines taches lancées par défaut, en commentant la ligne de certain path : this.HBS, this.SASS_BS_CUSTOM et this.INJECTORS

Voir les commentaires dans le fichier `application.conf.js`

#### Editer les constants JS pour la génération des fichiers suivants :

- FILES_VENDOR_CSS pour générer `vendor.css`
- FILES_VENDOR_JS pour générer `vendor.js`
- FILES_APPLICATION_JS pour générer `application.js`

#### Definir la copie de fichiers assets (images, fonts, polyfills) 

- définir la liste des fichiers dans `ASSETS_TO_COPY`
. 

Chaque objet du tableau contient les attributs suivants :
- files : chemins des fichiers
- ouputDir : chemin de destination des assets
- rewritingBaseUrl : Réecriture de l'url de base d'un fichier CSS temporaire

Pour les fichiers JS uniquement :
- stripDebug: true/false => Suppression des console.log(), alert() et debugger
- uglify: true/false => Optimisation JS (syntaxe ES6 non reconnue) 
- banner: true/false => Ajoute la bannière en début de fichier


Pour les fichiers CSS uniquement :
```
rewritingBaseUrl : 
                        {   
                            fileTempCss:  'styles-vendor.css',
                            newBaseUrl: '../css/vendor/select2/',
                            options: { logs: { enabled: false } }
                        }
```

- On peut au préalable supprimer les assets existant en utilisant le tableau `ASSETS_TO_DELETE`


#### lister les fichiers à surveiller dans `FILES_TO_WATCH`
- files : liste des fichiers
- tasks : tableau des taches à executer lors du changement des fichiers
- ajouter event : "FORCE_RELOAD" pour forcer la navigateur à se rafraichir 
(par default, seule une injection CSS est réalisée sans rechargement de la page)

#### Injecter automatiquement les tags `<script>` et `<link>`

Vous pouvez définir le nom des fichiers serveurs à créer contenant l'injection des tags spécifiques (CSS ou JS) en editant la constante suivante :

```
INJECTORS : [
        {
            'name' : 'css-main',
            'serverFilePath' : PATHS.INJECTORS + 'injector-css-main.php',
            'baseUrl' : '<?= BASENAME; ?>css/',
            'files' : PATHS.CSS_ROOT + '*.css',
            'aOrderedFiles' : ["*vendor*", "*bootstrap*", "*"]
        },
        {
            'name' : 'js-main',
            'serverFilePath' : PATHS.INJECTORS + 'injector-js-main.php',
            'baseUrl' : '<?= BASENAME; ?>js/',
            'files' : PATHS.JS_ROOT + '*.js',
            'aOrderedFiles' : ["*vendor*", "*bootstrap*", "*"]
        },
        {
            'name' : 'js-polyfills',
            'serverFilePath' : PATHS.INJECTORS + 'injector-js-polyfills.php',
            'baseUrl' : '<?= BASENAME; ?>js/vendor/polyfills/',
            'files' : PATHS.JS_POLYFILLS + '*.js',
            'aOrderedFiles' : ["*vendor*", "*bootstrap*", "*"]
        }
    ]
```

Les fichiers *injectors* sont à part car ils sont ignorés de git, pour éviter un commit lors de chaque build.

##### Détail des attributs :
- *name* : ne sert qu'à nommer l'injecteur

- *serverFilePath* : le chemin du fichier qui doit être crée dans l'application

- *baseUrl* : le chemin de base à injecter dans l'attribut href

- *files* : l'emplacement des fichiers à injecter

- *aOrderedFiles* : un tableau contenant les noms des fichiers à ordonner
il est possible d'utiliser le caractère joker "*" pour par exemple faire apparaitre vendor en premier quelque soit sa syntaxe générée par le build (vendor.js, vendor.min.js ou vendor-1460035660.min.css)


#### Définir des taches supplémentaires pour la version de production :

- Ajouter une concaténation supplémentaire dans `FILES_TO_CONCAT`
- Lister les fichiers à revisionner dans `FILES_TO_REVISION`

## Définir un configuration personnalisée pour BrowserSync

- Dupliquer le fichier `browser-sync.conf.global.js` en `browser-sync.conf.local.js`
- Editer ce fichier pour définir un numéro de port différent ou un navigateur autre que chrome.

D'autres options sont disponibles, voir les commentaires du fichier pour en savoir plus.


## Ajout de tâche dans le menu de démarrage
Toutes tâches disposant d'une description apparait dans le menu de démarrage de gulp. 
Les tâches apparaissent par ordre alphabétique (non modifiable)

## Forcer une version de Gulpackmen
Il suffit de spécifier le numéro de version dans l'url :
"@exocety/gulpackmen": "@exocety/gulpackmen@1.0.1"

