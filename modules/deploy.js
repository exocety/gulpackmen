module.exports = function(gulp, conf, $) {

    var development         = $.environments.development;
    var production          = $.environments.production;

    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    // Message de démarrage de la tâche DEPLOY
    gulp.task('deploy-msg', function (done) {
        $.log( $.chalk.yellow("========== 4. Déploiement de l'appli en PROD ==============================") );
        done();
    });

    // Optimisation des fichiers CSS et copie vers public
    gulp.task('copy-optimize-css', function (done) {
        if(production() ){
            $.log("-------------------- OPTIMISATION CSS -------------------------------------");    
            return gulp.src( conf.PATHS.TEMP_CSS + "**/*.css" )
                .pipe( $.size({title:"<--", showFiles: true, showTotal: false}) )
                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )  
            
                .pipe( production ( $.postcss([ $.cssnano( {zindex: false} ) ]) ) )
                
                .pipe( $.header(conf.BANNER, {pkg: conf.PACKAGE}) )

                .pipe( production ( $.rename(function (path) {
                    path.extname = ".min.css";
                })) )
                
                .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )
                .pipe( gulp.dest( conf.PATHS.CSS_ROOT ) )

                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
                .pipe( $.size({title: 'Copie vers "' + conf.PATHS.CSS_ROOT + '"' , showFiles: false, showTotal: true}) );
        }else{
            $.log( $.chalk.gray("-------------------- AUCUNE OPTIMISATION CSS ------------------------------") );
            done();
        }
    });

    // Optimisation des fichiers JS et copie vers public
    gulp.task('copy-optimize-js', function (done) {
        if(production() ){      
            $.log("-------------------- OPTIMISATION JS --------------------------------------");    
            return gulp.src( conf.PATHS.TEMP_JS + "**/*.js" )
                .pipe( $.size({title:"<--", showFiles: true, showTotal: false}) )
                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
                
                .pipe( production( $.stripDebug() ) )
                .pipe( production( $.uglify() ) )

                .pipe( $.header(conf.BANNER, {pkg: conf.PACKAGE}) )

                .pipe( production ( $.rename(function (path) {
                    path.extname = ".min.js";
                })) )
                
                .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )               
                .pipe( gulp.dest( conf.PATHS.JS_ROOT ) )
                
                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
                .pipe( $.size({title: 'Copie vers "' + conf.PATHS.JS_ROOT + '"' , showFiles: false, showTotal: true}) );               
        }else{
            $.log( $.chalk.gray("-------------------- AUCUNE OPTIMISATION JS -------------------------------") );
            done();
        }
    });

    // Copie des fichiers de temp CSS vers public
    gulp.task('copy-all-temp-css', function () {
        $.log("-------------------- COPIE DES FICHIERS CSS TEMP VERS PUBLIC --------------");
        return gulp.src( conf.PATHS.TEMP_CSS + "**/*" )
            .pipe( gulp.dest( conf.PATHS.CSS_ROOT ) );
    });

    // Copie des fichiers de temp JS vers public
    gulp.task('copy-all-temp-js', function () {
        $.log("-------------------- COPIE DES FICHIERS JS TEMP VERS PUBLIC ---------------");
        return gulp.src( conf.PATHS.TEMP_JS + "**/*" )
            .pipe( gulp.dest( conf.PATHS.JS_ROOT ) );
    });

    // Copie des fichiers de JS SRC vers public JS
    gulp.task('copy-all-js-src', function () {
        $.log("-------------------- COPIE DES FICHIERS JS SRC VERS PUBLIC ---------------");
        return gulp.src( conf.PATHS.JS_APP_SRC + "**/*" )
            .pipe( gulp.dest( conf.PATHS.JS_APP ) );
    });


    // Liste des tâches de déploiement
    gulp.task( "deploy",
                function(callback) {
                    if( development() ) {
                        gulp.series(    'deploy-msg', 
                                        'delete-assets',    // Dépendance à copy-assets
                                        'copy-assets',      // Dépendance à url-rewriting
                                        'copy-all-temp-css', 
                                        'copy-all-temp-js'
                                    )(callback);
                    } else {
                        gulp.series(    'deploy-msg',
                                        'delete-assets',    // Dépendance à copy-assets
                                        'copy-assets',      // Dépendance à url-rewriting
                                        'concat',
                                        'clean-concat',
                                        'copy-optimize-css',
                                        'copy-optimize-js',
                                        'revision',
                                        'clean-revision'
                                    )(callback);
                    }
                }
    );
};