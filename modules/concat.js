module.exports = function(gulp, conf, $) {
    
    var development         = $.environments.development;

    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    gulp.task('concat', function (done) {
        if( conf.FILES_TO_CONCAT && conf.FILES_TO_CONCAT.length ){
            $.log("-------------------- CONCATENATION ----------------------------------------");

            var tasks = conf.FILES_TO_CONCAT.map(function (task) {
                return gulp.src( task.files )
                            .pipe( $.size({title:"<--", showFiles: true, showTotal: false}) )
                            .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )

                            .pipe( development($.sourcemaps.init()) )

                            .pipe( $.concat(task.outputFileName ) ) 

                            .pipe( development($.sourcemaps.write('.')) )

                            .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )
                            .pipe( gulp.dest( task.outputDir ) )
                    
                            .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) );
            });
            
            return $.merge(tasks);
        }else{
            $.log( $.chalk.gray("-------------------- AUCUNE CONCATENATION ---------------------------------") );
            done();
        }
    });


    gulp.task('clean-concat', function (done) {
        if( conf.FILES_TO_CONCAT && conf.FILES_TO_CONCAT.length ){
            
            var files = conf.FILES_TO_CONCAT.map(function (task) {
                if(task.files.length > 0){
                    task.files.map(function (files, index) {
                        if(index === 0){
                            $.log("Suppression de : " + $.chalk.blue(files) );
                        }else{

                            $.log("               : " + $.chalk.blue(files) );
                        }
                    });
                }else{
                    $.log("Suppression de : " + task.files );
                }
                return task.files.toString();
            }).toString();
            var aFilesToDelete = files.split(",");
            return $.del(aFilesToDelete, {force: true} );

        }else{
            done();
        }
    });
};