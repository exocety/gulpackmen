module.exports = function(gulp, conf, $) {
    // --------------------------------------------------------------
    // Installation du répertoire de travail 
    // et des fichiers de paramétrage de Gulpackmen
    // --------------------------------------------------------------
    gulp.task( "gulpackmen:folder", function(){       
        $.log("------------- Copie du répertoire GULPACK (.temp + conf) -------------------");
        return gulp.src([
            './node_modules/@exocety/gulpackmen/gulpackmen/**/*',
            './node_modules/@exocety/gulpackmen/gulpackmen/.gitignore',
        ])
        .pipe(gulp.dest('./gulpackmen'));
    });

    gulp.task( "gulpackmen:gulpfile", function(){
        $.log("------------- Modification du Gulpfile -------------------------------------");
        return gulp.src([           
            './node_modules/@exocety/gulpackmen/gulpfile.js',
        ])
        .pipe(gulp.dest('./'));
    });

    gulp.task( "default", gulp.parallel("gulpackmen:folder", "gulpackmen:gulpfile") );
};