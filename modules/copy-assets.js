module.exports = function(gulp, conf, $) {

    var production          = $.environments.production;

    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    gulp.task('copy-assets', function (done) {
        if( conf.ASSETS_TO_COPY && conf.ASSETS_TO_COPY.length ){              
            $.log("-------------------- COPIE DES ASSETS -------------------------------------");

            var tasks = conf.ASSETS_TO_COPY.map(function (task) {
                task.files.map(function (files, index) {
                    if(index === 0){
                        $.log("Copie de : " + $.chalk.blue(files) );
                    }else{
                        $.log("         : " + $.chalk.blue(files) );
                    }
                });
                
                var streamCopy = gulp.src(task.files);

                // Création d'un stream temporaire contenant le stream de copie de fichiers
                // Nettoyage console, alert, and debugger dans les fichiers copiés si mode PROD
                if(task.stripDebug === true){
                    streamCopy.pipe( production( $.stripDebug() ) );
                    $.log("    avec stripDebug (prod)");
                }
                if(task.uglify === true){
                    streamCopy.pipe( production( $.uglify() ) );
                    $.log("    avec uglify (prod)");
                }
                if(task.banner === true){
                    streamCopy.pipe( $.header(conf.BANNER, {pkg: conf.PACKAGE}) );
                    $.log("    avec banner (prod)");
                }

                streamCopy.pipe(gulp.dest(task.outputDir));

                var stream = $.merge( streamCopy );

                $.log("    vers : " + task.outputDir );

                // Modification des url dans le fichier CSS si nécessaire
                if( task.rewritingBaseUrl ) { 
                    var rewrite = task.rewritingBaseUrl;
                    $.log( $.chalk.green(" new url : " + task.outputDir) );
                    $.log( $.chalk.green("    dans : " + rewrite.fileTempCss) );
                    
                    // Ajout au stream temporaire du stream de réecriture d'url
                    stream.add(
                        gulp.src(conf.PATHS.TEMP_CSS + rewrite.fileTempCss)
                            .pipe( $.replace( /url\('(?!\.)/g , 'url(\''+rewrite.newBaseUrl , rewrite.options ) )
                            .pipe( gulp.dest(conf.PATHS.TEMP_CSS) )
                    );
                }

                return stream;
            });
            
            return $.merge(tasks);
        
        }else{
            done();
        }
    });

    // Supprime tous les fichiers specifiés
    gulp.task('delete-assets', function (done) {
        if( conf.ASSETS_TO_DELETE && conf.ASSETS_TO_DELETE.length ){              
            $.log("-------------------- SUPPRESSION DES ASSETS -------------------------------");
            conf.ASSETS_TO_DELETE.map(function (assets) {
                $.log("Assets   : " + assets);
            });
            return $.del( conf.ASSETS_TO_DELETE, {force: true} );
        }else{
            done();
        }

    });
};