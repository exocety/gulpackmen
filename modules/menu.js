// inpired from https://github.com/thyagoluciano/gulp-task-menu

module.exports = function(gulp, conf, $) {
    
    // --------------------------------------------------------------
    // Menu de lancement
    // --------------------------------------------------------------

    gulp.task( "menu", _menu );

    function _menu(done) {
        var choices = [];
        var nbSpaces = 20;

        Object.keys( gulp.registry().tasks() ).map( function(k) {
            var oTask = gulp.registry().tasks()[k];
            if(oTask.description){
                var space = "";
                for (var i = 0; i < nbSpaces - oTask.displayName.length; i++) { 
                    space += " "; 
                }
                choices.push("" + oTask.displayName + " " + space + " > " + oTask.description );
            }
            return true;
         });

        choices.sort();
        choices.push('-- Quitter --');

        var question = {
            type: "list",
            name: "gulp",
            message: "Quelle tâche souhaitez-vous lancer ?",
            choices: choices
        };

        $.inquirer.prompt([question]).then(answers => {
            if (answers.gulp != '-- Quitter --') {           
                var aAnswer = answers.gulp.split(">");
                var taskName = aAnswer[0].trim();
                gulp.series(taskName)(done);
            }
            done();
        });
    }
};