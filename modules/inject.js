module.exports = function(gulp, conf, $) {
    
    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------
    
    // Message de démarrage de la tâche INJECT
    gulp.task('inject-msg', function (done) {
        $.log( $.chalk.yellow("========== 5. Injection des scripts/styles ================================") );
        done();
    });

    gulp.task('injectors', function (done) {
        if( conf.PATHS.INJECTORS && conf.INJECTORS ){              
            // $.log("-------------------- INJECTION DES SCRIPTS/STYLES -------------------------");

            var tasks = conf.INJECTORS.map(function (injector) {

                require("fs").writeFileSync(injector.serverFilePath, 
                    "<!-- inject -->\n<!-- endinject -->"
                );

                var target = gulp.src(injector.serverFilePath);
                var sources = gulp.src(injector.files, {read: false})
                                .pipe( $.order( injector.aOrderedFiles ) );
                var options = { 
                                starttag: '<!-- inject -->',
                                endtag: '<!-- endinject -->',
                                removeTags: true,
                                transform: function (filepath, file) {
                                    if(file.extname === ".css") {
                                        var media = (injector.media)?injector.media:"all";
                                        return '<link rel="stylesheet" href="'+ injector.baseUrl + file.basename +'" media="'+ media +'" type="text/css" />';
                                    }else{
                                        return '<script src="'+ injector.baseUrl + file.basename +'"></script>';
                                    }
                                }
                            };
                return target.pipe( $.inject(sources, options) )
                            .pipe(gulp.dest(conf.PATHS.INJECTORS));
            });
            
            return $.merge(tasks);
        
        }else{
            $.log( $.chalk.gray("-------------------- INJECTION DES SCRIPTS/STYLES DESACTIVÉ -------------------------") );
            done();
        }
    });

    gulp.task('inject', gulp.series('inject-msg', 'injectors'));
};