module.exports = function(gulp, conf, $) {

    var production          = $.environments.production;

    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    // Applique le numéro de révision et génère le manifest
    gulp.task("revision", function(done){
        if( conf.FILES_TO_REVISION && conf.FILES_TO_REVISION.length && production() ){
            $.log("-------------------- REVISION ---------------------------------------------");

            var files = conf.FILES_TO_REVISION.map(function (path) {
                path.files.map(function (files, index) {
                    if(index === 0){
                        $.log("Cache-busting de : " + $.chalk.blue(files) );
                    }else{

                        $.log("                 : " + $.chalk.blue(files) );
                    }
                });
                return path.files.toString();
            }).toString();
            var aFilesToRevision = files.split(",");

            return gulp.src(aFilesToRevision, {base: conf.PATHS.PROD})
                    .pipe(gulp.dest(conf.PATHS.PROD))  // copy original assets to build dir
                    .pipe( $.rev() )
                    .pipe(gulp.dest(conf.PATHS.PROD))  // write rev'd assets to build dir
                    .pipe( $.rev.manifest() )
                    .pipe(gulp.dest(conf.PATHS.TEMP));  // write manifest to build dir
        }else{
            $.log( $.chalk.gray("-------------------- REVISION DESACTIVÉ -----------------------------------") );
            done();
        }
    });

    gulp.task('clean-revision', function (done) {
        if( conf.FILES_TO_REVISION && conf.FILES_TO_REVISION.length && production() ){

            var files = conf.FILES_TO_REVISION.map(function (path) {
                return path.files.toString();
            }).toString();
            var aFilesToDelete = files.split(",");

            $.log( $.chalk.blue("Suppression des " + aFilesToDelete.length + " anciens fichiers" ) );

            return $.del(aFilesToDelete, {force: true} );

        }else{
            done();
        }
    });
};