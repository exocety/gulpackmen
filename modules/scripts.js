module.exports = function(gulp, conf, $) {

    var development         = $.environments.development;

    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    // Message de démarrage de la tâche scripts-all
    gulp.task('scripts-msg', function (done) {
        $.log( $.chalk.yellow("========== 3. Génération des fichiers JS ==================================") );
        done();
    });

    // Traitement des scripts app
    gulp.task('scripts-app', function(done) {
        if( conf.FILES_APPLICATION_JS && conf.FILES_APPLICATION_JS.length ){
            $.log('-------------------------- GENERATION APPLICATION.JS ----------------------');
            return gulp
                .src( conf.FILES_APPLICATION_JS )
                .pipe( $.size({title:"<--", showFiles: true, showTotal: false}) )
                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )

                .pipe( development($.sourcemaps.init()) )
                .pipe( $.concat('application.js') )
                .pipe( development($.sourcemaps.write('.')) )

                .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )
                .pipe( gulp.dest(conf.PATHS.TEMP_JS) )

                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
                .pipe( $.size({title: 'Copie vers "' + conf.PATHS.TEMP_JS + '"' , showFiles: false, showTotal: true}) );
        }else{		
            $.log('-------------------------- AUCUN FICHIER APPLICATION.JS A GENERER ---------');
            done();
        }   
    });

    // Rebuild de l'application.js
    gulp.task('scripts-app-dev', function (done) {
        if( conf.FILES_APPLICATION_JS && conf.FILES_APPLICATION_JS.length ){
            $.log('-------------------------- REBUILD APPLICATION JS -------------------------');
            return gulp
                .src( conf.FILES_APPLICATION_JS )
                .pipe( $.size({title:"<--", showFiles: true, showTotal: false}) )
                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )

                .pipe( development($.sourcemaps.init()) )
                .pipe( $.concat('application.js') )
                .pipe( development($.sourcemaps.write('.')) )

                .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )
                .pipe( gulp.dest(conf.PATHS.JS_ROOT) )

                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
                .pipe( $.size({title: 'Copie vers "' + conf.PATHS.JS_ROOT + '"' , showFiles: false, showTotal: true}) );
        }else{		
            $.log('-------------------------- AUCUN FICHIER APPLICATION.JS A GENERER ---------');
            done();
        }
    });

    // Traitement des scripts vendor
    gulp.task('scripts-vendor', function(done) {
        if( conf.FILES_VENDOR_JS && conf.FILES_VENDOR_JS.length ){           
            $.log('-------------------------- GENERATION VENDOR.JS ---------------------------');
            return gulp
                .src( conf.FILES_VENDOR_JS )
                .pipe( $.size({title:"<--", showFiles: true, showTotal: false}) )
                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )

                .pipe( development($.sourcemaps.init()) )
                .pipe( $.concat('vendor.js') )
                .pipe( development($.sourcemaps.write('.')) )

                .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )
                .pipe( gulp.dest(conf.PATHS.TEMP_JS) )

                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
                .pipe( $.size({title: 'Copie vers "' + conf.PATHS.TEMP_JS + '"' , showFiles: false, showTotal: true}) );
        }else{		
            $.log('-------------------------- AUCUN FICHIER VENDOR.JS A GENERER --------------');
            done();
        }
    });

    // Traitement de tous les styles
    gulp.task('scripts-all', gulp.series('scripts-msg', 'scripts-app', 'scripts-vendor', 'compile-hbs') ); // Dépendance vers handlebars.js
};