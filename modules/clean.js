
module.exports = function(gulp, conf, $) {
    
    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    // Supprime les fichiers sourcemaps css
    gulp.task('clean-all-sourcemap', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers .map") );
        return $.del( [
            conf.PATHS.TEMP_CSS + '**/*.map', 
            conf.PATHS.TEMP_JS + '**/*.map',
            conf.PATHS.CSS_ROOT + '**/*.map', 
            conf.PATHS.JS_ROOT + '**/*.map'
        ] , {force: true} );
    });

    // Supprime les fichiers json
    gulp.task('clean-all-json', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers .json") );
        return $.del( [
            conf.PATHS.TEMP_CSS + '**/*.json', 
            conf.PATHS.TEMP_JS + '**/*.json',
            conf.PATHS.TEMP + '*.json',
            conf.PATHS.CSS_ROOT + '**/*.json', 
            conf.PATHS.JS_ROOT + '**/*.json'
        ] , {force: true} );
    });

    // Supprime le rev-manifest
    gulp.task('clean-rev-json', function () {
        $.log( $.chalk.blue("Suppression du rev-manifest.json") );
        return $.del( [conf.PATHS.TEMP + 'rev-manifest.json'] , {force: true} );
    });

    // Supprime tous les fichiers CSS (+map) public
    gulp.task('clean-css', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers CSS (+map) public") );
        return $.del( [ conf.PATHS.CSS_ROOT + '**/*.css', conf.PATHS.CSS_ROOT + '**/*.map' ] , {force: true} );
    });

    // Supprime tous les fichiers JS (+map) public
    gulp.task('clean-js', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers JS (+map) public") );       
        return $.del( [ conf.PATHS.JS_ROOT + '**/*.js', conf.PATHS.JS_ROOT + '**/*.map' ] , {force: true} );
    });

    // Supprime tous les fichiers JS (+map) Vendor public
    gulp.task('clean-js-vendor', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers JS (+map) Vendor public") );
        return $.del( [ conf.PATHS.JS_VENDOR + '**/*.js', conf.PATHS.JS_VENDOR + '**/*.map' ] , {force: true} );
    });

    // Supprime tous les fichiers JS (+map) temporaire
    gulp.task('clean-temp-js', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers JS (+map) temporaire") );
        return $.del( [ conf.PATHS.TEMP_JS + '**/*.js', conf.PATHS.TEMP_JS + '**/*.map' ] , {force: true} );
    });
    
    // Supprime tous les fichiers CSS temporaire
    gulp.task('clean-temp-css', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers CSS (+map) temporaire") );
        return $.del( [ conf.PATHS.TEMP_CSS + '**/*.css', conf.PATHS.TEMP_CSS + '**/*.map' ] , {force: true} );
    });

    // Supprime tous les fichiers du réportoire temporaire JS
    gulp.task('clean-dir-temp-js', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers du répertoire temporaire JS") );
        return $.del( conf.PATHS.TEMP_JS + '**/*' , {force: true} );
    });
    
    // Supprime tous les fichiers réportoire temporaire CSS
    gulp.task('clean-dir-temp-css', function () {
        $.log( $.chalk.blue("Suppression de tous les fichiers du répertoire temporaire CSS") );
        return $.del( conf.PATHS.TEMP_CSS + '**/*' , {force: true} );
    });
    
    // Supprime tous les fichiers particuliers
    gulp.task('clean-all-specific', gulp.series('clean-rev-json', 'clean-all-sourcemap', function (done) {       
        $.log("-------------------------- RESET SPECIFIQUE EFFECTUE ! -----------------------------");
        done();
    }));

    // Supprime tous les fichiers CSS
    gulp.task('clean-all-css', gulp.series('clean-temp-css','clean-css', function (done) {
        $.log("-------------------------- RESET CSS EFFECTUE ! -----------------------------");
        done();
    }));
    
    // Supprime tous les fichiers JS
    gulp.task('clean-all-js', gulp.series('clean-temp-js','clean-js', 'clean-js-vendor', function (done) {
        $.log("-------------------------- RESET JS EFFECTUE ! -----------------------------");
        done();
    }));

    // Supprime tous les fichiers JS et CSS temporaire ou de build 
    gulp.task('clean-all', gulp.series('clean-rev-json', 'clean-all-css', 'clean-all-js', function (done) {
        $.log("-------------------------- RESET TERMINE ! --------------------------------");
        done(); 
    }));

};
