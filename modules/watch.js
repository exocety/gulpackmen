module.exports = function(gulp, conf, $) {
    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    // Surveillance des modifications de fichiers
    gulp.task('watch', function(done) {
        if( conf.FILES_TO_WATCH && conf.FILES_TO_WATCH.length ){
            // On surveille les fichiers déclarés dans le fichier de configuration
            conf.FILES_TO_WATCH.forEach(function(watch) {
                if( watch.event === "FORCE_RELOAD") {
                    if(watch.tasks) {
                        gulp.watch( watch.files, gulp.series(watch.tasks) ).on('change', global.browserSync.reload );
                    }else{
                        gulp.watch( watch.files ).on('change', global.browserSync.reload );
                    }
                }else{
                    if(watch.tasks) {
                        gulp.watch( watch.files, gulp.series(watch.tasks) );
                    }
                }
            });
        }else{
            done();
        } 
    });
};
