module.exports = function(gulp, conf, $) {

    var development            = $.environments.development;  

    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    // Message de démarrage de la tâche styles-all
    gulp.task('styles-msg', function (done) {
        $.log( $.chalk.yellow("========== 2. Génération du CSS à partir des fichiers SASS ================") );
        done();
    });

    // Traitement des styles - compile SASS + autoprefix (dev : +sourcemap et prod : +compression)
    gulp.task('styles-app', function () {
        $.log('-------------------------- GENERATION APPLICATION.CSS ---------------------');       
        return gulp
            .src( conf.PATHS.SASS_APP + 'application.scss' )
            .pipe( $.size({title:"<--", showFiles: true, showTotal: false}) )
            .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )

            .pipe( development($.sourcemaps.init()) )
            
            .pipe( $.sass({includePaths: [conf.PATHS.SASS_APP]}) )
            
            .pipe( $.postcss([ $.autoprefixer({ browsers: conf.AUTOPREFIXER_BROWSERS }) ]) )

            .pipe( development($.sourcemaps.write('.')) ) // '.' permet d'écrire les sourcemaps dans un fichier séparé dans le même répertoire
            

            .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )

            .pipe( gulp.dest(conf.PATHS.TEMP_CSS) )
            
            .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
            .pipe( $.size({title: 'Copie vers "' + conf.PATHS.TEMP_CSS + '"' , showFiles: false, showTotal: true}) );
    });
    

    // Traitement des styles - compile SASS + autoprefix UNIQUEMENT pour le DEV pour injection CSS
    gulp.task('styles-app-dev', function () {
        $.log('-------------------------- REBUILD APPLICATION CSS --------------------------');
        return gulp
            .src( conf.PATHS.SASS_APP + 'application.scss' )

            .pipe( $.sourcemaps.init() )
            
            .pipe( $.sass({includePaths: [conf.PATHS.SASS_APP]}) )
            
            .pipe( $.postcss([ $.autoprefixer({ browsers: conf.AUTOPREFIXER_BROWSERS }) ]) )        
            .pipe( $.sourcemaps.write('.') ) // '.' permet d'écrire les sourcemaps dans un fichier séparé dans le même répertoire

            .pipe( gulp.dest(conf.PATHS.CSS_ROOT) )

            // Injection du CSS après chaque changement de style
            .pipe( global.browserSync.stream({match: '**/*.css'}) );
    });
    

    // Traitement des styles vendor (dev : +sourcemap et prod : +compression)
    gulp.task('styles-vendor', function (done) {
        if( conf.FILES_VENDOR_CSS && conf.FILES_VENDOR_CSS.length ){
            $.log('-------------------------- GENERATION STYLES-VENDOR.CSS -------------------');
            return gulp
                .src( conf.FILES_VENDOR_CSS )
                .pipe( $.size({title:"<--", showFiles: true, showTotal: false}) )
                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )

                .pipe( development($.sourcemaps.init()) )
                
                .pipe( $.postcss([ $.autoprefixer({ browsers: conf.AUTOPREFIXER_BROWSERS }) ]) )

                .pipe( $.concat('styles-vendor.css') )
                .pipe( development( $.sourcemaps.write('.')) ) // '.' permet d'écrire les sourcemaps dans un fichier séparé dans le même répertoire

                .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )
                .pipe( gulp.dest( conf.PATHS.TEMP_CSS ) )
                
                .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
                .pipe( $.size({title: 'Copie vers "' + conf.PATHS.TEMP_CSS + '"' , showFiles: false, showTotal: true} ) );       
        }else{
            $.log('-------------------------- AUCUN FICHIER STYLES-VENDOR.CSS A BUILDER ------');
            done();
        }            
    });

    // Traitement du build custom de bootstras sass
    gulp.task('styles-bs-custom', function (done) {
        if( conf.PATHS.SASS_BS_CUSTOM ){
            $.log('-------------------------- GENERATION BOOTSTRAP-CUSTOM.CSS ----------------');
        return gulp
            .src( conf.PATHS.SASS_BS_CUSTOM + 'bootstrap.scss')
            .pipe( $.size({title:"<--", showFiles: true, showTotal: true}) )
            .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )

            .pipe( development($.sourcemaps.init()) )

            .pipe( $.sass({includePaths: [conf.PATHS.BOOTSTRAP_SRC]}) )
            
            .pipe( $.rename('bootstrap-custom.css') )
            .pipe( development( $.sourcemaps.write('.')) ) // '.' permet d'écrire les sourcemaps dans un fichier séparé dans le même répertoire
            
            .pipe( $.size({title:"-->", showFiles: true, showTotal: false}) )
            .pipe( gulp.dest( conf.PATHS.TEMP_CSS ) )
            
            .pipe( $.size({title: '---------------------------------', showFiles: false, showTotal: true}) )
            .pipe( $.size({title: 'Copie vers "' + conf.PATHS.TEMP_CSS + '"' , showFiles: false, showTotal: true}) );
		}else{		
            $.log( $.chalk.gray('-------------------------- BOOTSTRAP CUSTOM DESACTIVÉ ---------------------') );
            done();
        }
    });

    // Traitement de tous les styles
    gulp.task( "styles-all", gulp.series('styles-msg', 'styles-app', 'styles-vendor', 'styles-bs-custom') );
};