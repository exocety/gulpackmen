module.exports = function(gulp, conf, $) {	
    
    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    // To check your project from security fails
    gulp.task('nsp', function (cb) {
        // $.nsp({package: process.cwd() + '/package.json'}, cb );
        $.log( $.chalk.blue( "Le module NSP a été désactivé car il est obsolète !") );
        $.log( $.chalk.green( "Veuillez utiliser l'audit intégré à npm ou yarn") );
        cb();
    });
};