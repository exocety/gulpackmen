module.exports = function(gulp, conf, $) {

    var development         = $.environments.development;
    var production          = $.environments.production;	
    
    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------
    
    // Affiche l'environnement
    gulp.task('getENV', getEnv );

    function getEnv(done) {
        var dev = development() ? "true" : "false";
        var prod = production() ? "true" : "false";
        $.log("ENV ---- DEV:" + dev +" PROD:"+ prod );
        done();
    }
    
    // Set la variable d'environnement à DEV
    gulp.task('setDEV', gulp.series( setDev, 'clean-all' ) );
    
    function setDev(done) {
        $.environments.current(development); 
        $.log( $.chalk.yellow("======== 1. Build en mode DEV =============================================") );
        done();
    }
    
    // Set la variable d'environnement à PROD
    gulp.task('setPROD', gulp.series( setProd, 'clean-all'  ) );

    function setProd(done) {
        $.environments.current(production);
        $.log( $.chalk.yellow("======== 1. Build en mode PROD ============================================") );
        done();
    }

    // Get version number
    gulp.task('version', gulp.series( getVersion) );

    function getVersion(done) {
        if (require('fs').existsSync('./gulpackmen/conf/application.conf.js')) {
            conf.VERSION = require( process.cwd() + '/node_modules/@exocety/gulpackmen/package.json').version;
            $.log( $.chalk.yellow("version " + conf.VERSION) );
        }else{
            $.log( "Numéro de version indisponible" );
        }
        done();
    }
};