module.exports = function(gulp, conf, $) {	
  
    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------

    // Compile les partials HBS
    gulp.task('compile-partials', function(done) {  
        if( conf.PATHS.HBS ){
            $.log("-------------------- COMPILATION HBS PARTIALS -----------------------------");
            return gulp.src( conf.PATHS.HBS + 'partials/*.hbs')
                .pipe( $.handlebars({
                    handlebars: require('handlebars')
                }))
                .pipe( $.wrap('Handlebars.registerPartial(<%= processPartialName(file.relative) %>, Handlebars.template(<%= contents %>));', {}, {
                    imports: {
                        processPartialName: function(fileName) {
                            return JSON.stringify($.path.basename(fileName, '.js'));
                        }
                    }
                }))
                .pipe( $.concat('partials.js') )
                .pipe(gulp.dest( conf.PATHS.TEMP_JS ))
                .pipe( global.browserSync.stream({match: '**/*.js'}) );

        }else{
            $.log( $.chalk.gray("-------------------- COMPILATION HBS PARTIALS DESACTIVÉ -------------------") );		
            done();
        }
    });

    // Compile les partials HBS directement vers PUBLIC (pour le watch !)
    gulp.task('compile-partials-dev', function(done) {  
        if( conf.PATHS.HBS ){
            $.log("-------------------- REBUILD HBS PARTIALS -----------------------------");
            return gulp.src( conf.PATHS.HBS + 'partials/*.hbs')
                .pipe( $.handlebars({
                    handlebars: require('handlebars')
                }))
                .pipe( $.wrap('Handlebars.registerPartial(<%= processPartialName(file.relative) %>, Handlebars.template(<%= contents %>));', {}, {
                    imports: {
                        processPartialName: function(fileName) {
                            return JSON.stringify($.path.basename(fileName, '.js'));
                        }
                    }
                }))
                .pipe( $.concat('partials.js') )
                .pipe(gulp.dest( conf.PATHS.JS_ROOT ))
                .pipe( $.size({title: 'Copie vers "' + conf.PATHS.JS_ROOT + '"' , showFiles: true}) )
                .pipe( global.browserSync.stream({match: '**/*.js'}) );

        }else{
            $.log( $.chalk.gray("-------------------- COMPILATION HBS PARTIALS DESACTIVÉ -------------------") );		
            done();
        }
    });

    // Compile les fichers HBS
    gulp.task('compile-templates', function(done){
        if( conf.PATHS.HBS ){
            $.log("-------------------- COMPILATION HBS TEMPLATES ----------------------------");
            
            return gulp.src( conf.PATHS.HBS + '*.hbs')
                .pipe( $.handlebars({
                    handlebars: require('handlebars')
                }))
                .pipe( $.wrap('Handlebars.template(<%= contents %>)') )
                .pipe( $.declare({
                    namespace: 'Application.templates',
                    noRedeclare: true, // Avoid duplicate declarations 
                }))
                .pipe( $.concat('templates.js') )
                .pipe(gulp.dest( conf.PATHS.TEMP_JS ))
                .pipe( global.browserSync.stream({match: '**/*.js'} ) );

        }else{
            $.log( $.chalk.gray("-------------------- COMPILATION HBS TEMPLATES DESACTIVÉ ------------------") ); 
            done();
        }
    });

    // Compile les fichers HBS directement vers PUBLIC (pour le watch !)
    gulp.task('compile-templates-dev', function(done){
        if( conf.PATHS.HBS ){
            $.log("-------------------- REBUILD HBS TEMPLATES ----------------------------");
            
            return gulp.src( conf.PATHS.HBS + '*.hbs')
                .pipe( $.handlebars({
                    handlebars: require('handlebars')
                }))
                .pipe( $.wrap('Handlebars.template(<%= contents %>)') )
                .pipe( $.declare({
                    namespace: 'Application.templates',
                    noRedeclare: true, // Avoid duplicate declarations 
                }))
                .pipe( $.concat('templates.js') )
                .pipe(gulp.dest( conf.PATHS.JS_ROOT ))
                .pipe( $.size({title: 'Copie vers "' + conf.PATHS.JS_ROOT + '"' , showFiles: true}) )
                .pipe( global.browserSync.stream({match: '**/*.js'} ) );

        }else{
            $.log( $.chalk.gray("-------------------- COMPILATION HBS TEMPLATES DESACTIVÉ ------------------") ); 
            done();
        }
    });

    // Traitement de tous les styles
    gulp.task('compile-hbs', gulp.series('compile-templates', 'compile-partials') );
};
