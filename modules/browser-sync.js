
module.exports = function(gulp, conf, $) {

    // --------------------------------------------------------------
    // Liste des tâches
    // --------------------------------------------------------------
    
    function _launchBrowserSync() {
        // Chargement de la configuration locale de browserSync si elle existe
        if (require('fs').existsSync('./gulpackmen/conf/browser-sync.conf.local.js')) {
            $.log("------------- Chargement de la configuration LOCALE pour BrowserSync -------------------");
            conf.BROWSERSYNC_CONF = require( process.cwd() + '/gulpackmen/conf/browser-sync.conf.local.js');
        }else{
            $.log("------------- Chargement de la configuration GLOBALE pour BrowserSync -------------------");
            conf.BROWSERSYNC_CONF    = require( process.cwd() + '/gulpackmen/conf/browser-sync.conf.global.js');
        }
        $.log("configuration : " , conf.BROWSERSYNC_CONF);
        $.log("----------------------------------------------------------------------------------------");

        // Paramétres de lancement de BrowserSync
        global.browserSync.init(conf.BROWSERSYNC_CONF);
    }

    // Lancement du BrowserSync
    gulp.task('launchBrowserSync', _launchBrowserSync);

    // Arret du BrowserSync
    gulp.task('stopBrowserSync', function() {
        global.browserSync.exit();
    });
};