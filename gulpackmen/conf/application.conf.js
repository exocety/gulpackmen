
// --------------------------------------------------------------
// Imports des dépendances
// --------------------------------------------------------------

global.PACKAGE             = require('../../package.json');
global.GULPACKMEN_PACKAGE     = require('../../node_modules/@exocety/gulpackmen/package.json');
console.log("------------- Gulpackmen v"+ global.GULPACKMEN_PACKAGE.version +" - Chargement du fichier de configuration -----------------------------------");

// --------------------------------------------------------------
// Initialisation des chemins utilisés dans GULP
// --------------------------------------------------------------

var oPaths = function () {
    // GENERAL
    this.BUILD              = "./";                                                     // dossier de travail du build
    this.ROOT               = "../../";                                                 // dossier racine du projet
    this.FRONT_COMPONENTS   = this.BUILD               + 'node_modules/';               // dossier des composants front-end
    
    // TEMPORAIRE
    this.TEMP               = this.BUILD               + 'gulpackmen/.temp/';              // dossier des fichiers temporaires de build
    this.TEMP_CSS           = this.TEMP                + 'css/';                        // dossier des fichiers temporaires CSS de build
    this.TEMP_JS            = this.TEMP                + 'js/';                         // dossier des fichiers temporaires JS de build

    // SRC ASSETS
    this.SRC                = this.BUILD               + 'src/';                        // dossier contenant les sources à destination du répertoire public
    
    this.JS_SRC             = this.SRC                 + 'js/';                         // dossier racine du source JS
    // A commenter si on ne veux pas utiliser Handlebars
    this.HBS                = this.SRC                 + 'js/templates/';               // dossier des templates HBS
    this.FONTS_SRC          = this.SRC                 + 'fonts/';                      // dossier de build contenant les polices de caractères
    this.IMG_SRC            = this.SRC                 + 'img/';                        // dossier de build contenant les images

    // SASS
    this.BOOTSTRAP_ASSETS   = this.FRONT_COMPONENTS    + 'bootstrap-sass/assets/';      // dossier des assets de bootstrap-sass
    this.BOOTSTRAP_SRC      = this.BOOTSTRAP_ASSETS    + 'stylesheets/';                // dossier des styles de bootstrap-sass
    this.BOOTSTRAP_JS_SRC   = this.BOOTSTRAP_ASSETS    + 'javascripts/';                // dossier des javascripts de bootstrap-sass
    
    this.SASS_ROOT          = this.SRC                 + 'sass/';                       // dossier source du style SASS
    this.SASS_APP           = this.SASS_ROOT           + 'app/';                        // dossier source du style SASS
    // A commenter si on ne veux pas utiliser la version boostrap-custom
    this.SASS_BS_CUSTOM     = this.SASS_ROOT           + 'bootstrap-custom/';           // dossier source SASS pour customiser Bootstrap
    
    // LANGAGE SERVEUR
    this.SERVER_SRC         = this.ROOT                + 'module/';                     // dossier source du PHP
    this.LAYOUT_SRC         = this.SERVER_SRC          + 'Application/view/layout/';    // dossier source du layout.phtml
    // A commenter si on ne veux pas utiliser les injecteurs
    this.INJECTORS          = this.LAYOUT_SRC          + 'injectors/';                  // dossier contenant les injecteurs
    
    // PROD
    this.PROD               = this.ROOT                + 'public/';                     // dossier de prod
    
    this.CSS_ROOT           = this.PROD                + 'css/';                        // dossier de prod CSS
    this.CSS_VENDOR         = this.CSS_ROOT            + 'vendor/';                     // dossier des scripts CSS utilitaires (non buildé dans vendor.css)
    this.CSS_ADMIN          = this.CSS_VENDOR          + 'admin/';                      // dossier contenant des scripts css à importer uniquement si on est la profil admin
    
    this.JS_ROOT            = this.PROD                + 'js/';                         // dossier racine du JS
    this.JS_APP             = this.JS_ROOT             + 'app/';                        // dossier contenant les scripts JS de l'application
    this.JS_APP_SRC         = this.JS_SRC              + 'app/';                        // dossier contenant les sources des scripts JS de l'application
    this.JS_VENDOR          = this.JS_ROOT             + 'vendor/';                     // dossier des scripts JS utilitaires (non buildé dans vendor.js)
    this.JS_POLYFILLS       = this.JS_VENDOR           + 'polyfills/';                  // dossier des polyfills JS
    this.JS_ADMIN           = this.JS_VENDOR           + 'admin/';                      // dossier contenant des scripts js à importer uniquement si on est la profil admin
    
    this.IMG                = this.PROD                + 'img/';                        // dossier de prod des images
    this.FONTS              = this.PROD                + 'fonts/';                      // dossier de prod contenant les polices de caractères

};
var PATHS = new oPaths ();


// --------------------------------------------------------------
// Module de configuration
// --------------------------------------------------------------

module.exports = {
    // Initialise les chemins
    PATHS : PATHS,

    // Package.json
    PACKAGE : PACKAGE,

    // Url pour BrowserSync
    BROWSERSYNC_CONF : "",

    // Bandeau en entête de fichiers CSS et JS
    BANNER : [
    '/**',
    ' ** <%= PACKAGE.name %> - <%= PACKAGE.description %>',
    ' ** @author <%= PACKAGE.author %>',
    ' ** @version v<%= PACKAGE.version %>',
    ' **/',
    ''
    ].join('\n'),

    // Paramétrage par default de bootstrap 3
    AUTOPREFIXER_BROWSERS : [
        "Android 2.3",
        "Android >= 4",
        "Chrome >= 20",
        "Firefox >= 24",
        "Explorer >= 8",
        "iOS >= 6",
        "Opera >= 12",
        "Safari >= 6"
    ],

    // Paramétrage par default de bootstrap 4
    // AUTOPREFIXER_BROWSERS : [
    //     "last 1 major version",
    //     ">= 1%",
    //     "Chrome >= 45",
    //     "Firefox >= 38",
    //     "Edge >= 12",
    //     "Explorer >= 10",
    //     "iOS >= 9",
    //     "Safari >= 9",
    //     "Android >= 4.4",
    //     "Opera >= 30"
    // ],

    // Liste des fichiers CSS utilisés dans les web components
    FILES_VENDOR_CSS : [    
        PATHS.FRONT_COMPONENTS + 'bootstrap-toggle/css/bootstrap-toggle.min.css',
        PATHS.FRONT_COMPONENTS + 'select2/dist/css/select2.css',
        PATHS.FRONT_COMPONENTS + 'jquery-datetimepicker/jquery.datetimepicker.css'       
    ],

    // Liste des fichiers JS utilisés pour obtenir le fichier JS VENDOR
    FILES_VENDOR_JS : [
        PATHS.FRONT_COMPONENTS + 'jquery/dist/jquery.js',
        PATHS.BOOTSTRAP_JS_SRC + 'bootstrap.js',
        PATHS.FRONT_COMPONENTS + 'bootstrap-toggle/js/bootstrap-toggle.js',

        PATHS.FRONT_COMPONENTS + 'select2/dist/js/select2.js',
        PATHS.FRONT_COMPONENTS + 'select2/dist/js/i18n/fr.js',
        
        PATHS.FRONT_COMPONENTS + 'jquery-autosize/jquery.autosize.js',
        PATHS.FRONT_COMPONENTS + 'handlebars/dist/handlebars.runtime.min.js',
        
        PATHS.FRONT_COMPONENTS + 'jquery-form/jquery.form.js',
        PATHS.FRONT_COMPONENTS + 'jquery-mask-plugin/dist/jquery.mask.js',
        PATHS.FRONT_COMPONENTS + 'jquery-validation/dist/jquery.validate.js',
        PATHS.FRONT_COMPONENTS + 'jquery-validation/dist/localization/messages_fr.js',
        PATHS.FRONT_COMPONENTS + 'jquery-datetimepicker/build/jquery.datetimepicker.full.min.js',
    ],

    // Liste des fichiers JS utilisés pour obtenir le fichier JS APPLICATION
    FILES_APPLICATION_JS : [
        PATHS.JS_APP_SRC + 'common.js',
        PATHS.JS_APP_SRC + 'views/**/*.js'
    ],

    ASSETS_TO_DELETE : [
        PATHS.FONTS + '**/*',
        PATHS.IMG + '**/*',
        PATHS.CSS_ROOT + '**/*',
        PATHS.JS_ROOT + '**/*',
    ],
    
    ASSETS_TO_COPY : [
        { 
            // Copie des fonts bootstrap
            files:      [
                            PATHS.FRONT_COMPONENTS + 'bootstrap-sass/assets/fonts/bootstrap/*',
                        ],
            outputDir:  PATHS.FONTS + 'bootstrap'
        },
        { 
            // Copie des autres fonts utilisées
            files:      [
                            PATHS.FONTS_SRC + '**/*',
                        ],
            outputDir:  PATHS.FONTS
        },
        { 
            // Copie du javascript personnalisé pour l'application
            files:      [
                            PATHS.JS_APP_SRC + '**/*',
                        ],
            stripDebug: true,
            uglify: true,
            banner: true,
            outputDir:  PATHS.JS_APP
        },
        { 
            // Copie des polyfills JS
            files:      [ 
                            PATHS.FRONT_COMPONENTS + 'html5shiv/dist/html5shiv.min.js',
                            PATHS.FRONT_COMPONENTS + 'respond.js/dest/respond.min.js' 
                        ],
            stripDebug: true,
            outputDir:  PATHS.JS_POLYFILLS
        },
        { 
            // Copie des fichiers JS specialisés
            files:      [ 
                            PATHS.FRONT_COMPONENTS + 'bootstrap-fileinput/js/fileinput.min.js',
                            PATHS.FRONT_COMPONENTS + 'bootstrap-fileinput/js/locales/fr.js' 
                        ],
            stripDebug: true,
            outputDir:  PATHS.JS_ADMIN
        },
        { 
            // Copie des fichiers CSS specialisés
            files:      [ 
                            PATHS.FRONT_COMPONENTS + 'bootstrap-fileinput/css/fileinput.min.css'                           
                        ],
            outputDir:  PATHS.CSS_ADMIN
        },
        { 
            // Copie des images sans lancer l'optimisation
            files:      [ 
                            PATHS.IMG_SRC + '**/*'
                        ],
            outputDir:  PATHS.IMG
        },
        { 
            // Copie des images du SELECT 2
            files:      [ 
                            PATHS.FRONT_COMPONENTS + 'select2/*.gif',
                            PATHS.FRONT_COMPONENTS + 'select2/*.png'
                        ],
            outputDir:  PATHS.CSS_VENDOR + 'select2',
            rewritingBaseUrl : 
                        {   
                            fileTempCss:  'styles-vendor.css',
                            newBaseUrl: '../css/vendor/select2/',
                            options: { logs: { enabled: false } }
                        }
        }
    ],

    FILES_TO_WATCH : [
        { 
            files: PATHS.SASS_ROOT + '**/**/*.scss',
            tasks: ['styles-app-dev'],
        },
        { 
            files: PATHS.JS_APP_SRC + '**/*.js',
            tasks: ['scripts-app-dev'],
            event: "FORCE_RELOAD"
        },
        { 
            files: [ PATHS.SERVER_SRC + 'Application/**/*', 
                     '!' + PATHS.SERVER_SRC + 'Application/assets/**/*',
                   ],
            event: "FORCE_RELOAD"
        },
        { 
            files: PATHS.HBS + '*.hbs',
            tasks: ['compile-templates-dev'],
        },
        { 
            files: PATHS.HBS + 'partials/*.hbs',
            tasks: ['compile-partials-dev'],
        },
    ],

    INJECTORS : [
        {
            'name' : 'css-main',
            'serverFilePath' : PATHS.INJECTORS + 'injector-css-main.php',
            'baseUrl' : '<?= $this->basePath(); ?>/css/',
            'files' : PATHS.CSS_ROOT + '*.css',
            'media' : 'all',
            'aOrderedFiles' : ["*vendor*", "*bootstrap*", "*"]
        },
        {
            'name' : 'js-main',
            'serverFilePath' : PATHS.INJECTORS + 'injector-js-main.php',
            'baseUrl' : '<?= $this->basePath(); ?>/js/',
            'files' : PATHS.JS_ROOT + '*.js',
            'aOrderedFiles' : ["*vendor*", "*bootstrap*", "*"]
        },
        {
            'name' : 'js-polyfills',
            'serverFilePath' : PATHS.INJECTORS + 'injector-js-polyfills.php',
            'baseUrl' : '<?= $this->basePath(); ?>/js/vendor/polyfills/',
            'files' : PATHS.JS_POLYFILLS + '*.js',
            'aOrderedFiles' : ["*vendor*", "*bootstrap*", "*"]
        },
        {
            'name' : 'js-admin',
            'serverFilePath' : PATHS.INJECTORS + 'injector-js-admin.php',
            'baseUrl' : '<?= $this->basePath(); ?>/js/vendor/admin/',
            'files' : PATHS.JS_ADMIN + '*.js',
            'aOrderedFiles' : ["*fileinput*", "*"]
        }
    ],

    // --------------------------------------------------------------
    // Pour les fichiers de PROD uniquement
    // chaque fichier est optimisé et renommer avec le suffixe .min
    // Attention à la concaténation, le nouveau nom ne doit pas 
    // correspondre à un des noms des fichiers source (car ils sont effacés)
    // --------------------------------------------------------------

    // Liste des fichiers à concatener
    FILES_TO_CONCAT : [
        {
            files:  [ 
                    PATHS.TEMP_CSS    + 'bootstrap-custom.css',
                    PATHS.TEMP_CSS    + 'styles-vendor.css',
                    ],
            outputFileName: "vendor.css",
            outputDir: PATHS.TEMP_CSS
        },
        {
            files:  [ 
                    PATHS.TEMP_JS    + 'vendor.js',
                    PATHS.TEMP_JS    + 'templates.js',
                    PATHS.TEMP_JS    + 'partials.js',
                    ],
            outputFileName: "vendor-hbs.js",
            outputDir: PATHS.TEMP_JS
        },
    ],

    FILES_TO_REVISION : [
        { 
            files:  [
                    PATHS.CSS_ROOT + 'application.min.css',
                    PATHS.CSS_ROOT + 'vendor.min.css',
                    ],
            outputDir: PATHS.CSS_ROOT
        },
        {
            files:  [
                    PATHS.JS_ROOT + 'vendor-hbs.min.js',
                    PATHS.JS_ROOT + 'application.min.js',
                    ],
            outputDir: PATHS.JS_ROOT
        },
        {
            files:  [
                    PATHS.JS_POLYFILLS + 'html5shiv.min.js',
                    PATHS.JS_POLYFILLS + 'respond.min.js',
                    ],
            outputDir: PATHS.JS_POLYFILLS
        },
        {
            files:  [
                    PATHS.JS_ADMIN + 'fileinput.min.js',
                    PATHS.JS_ADMIN + 'fr.js',
                    ],
            outputDir: PATHS.JS_ADMIN
        }
        
    ]
};