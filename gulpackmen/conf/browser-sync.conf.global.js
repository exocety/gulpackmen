// Consulter la liste des options disponibles sur https://www.browsersync.io/docs/options

module.exports = {
    // lance le navigateur configuré par defaut, sinon on peut spécifier un navigateur en particulier
    browser:    ["chrome"],
    proxy:      "localhost/{applicationName}/",

    // open: false,             // Empêche l'ouverture d'un onglet à chaque redémarrage
    // reloadOnRestart: true,   // Reload à chaque redémarrage
};	

