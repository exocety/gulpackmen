'use strict';

// --------------------------------------------------------------
// Module GULPACKMEN
// --------------------------------------------------------------

module.exports = function (gulp, conf) {
    
    // --------------------------------------------------------------
    // Imports automatique des dépendances
    // --------------------------------------------------------------

    var $ = require('gulp-load-plugins')(
        {   pattern: '*', 
            rename: {
                'fancy-log': 'log',
                'merge-stream': 'merge',
                'gulp-string-replace': 'replace',
                'gulp-strip-debug': 'stripDebug',
            }
        }
    );
    
    // --------------------------------------------------------------
    // Liste des modules à utiliser
    // --------------------------------------------------------------
    function loadModuleTasks(moduleName) {
        return require("./modules/" + moduleName)(gulp, conf, $);
    }

    if (require('fs').existsSync('./gulpackmen/conf/application.conf.js')) {

        loadModuleTasks("clean");
        loadModuleTasks("env");
        loadModuleTasks("browser-sync");
        global.browserSync      = require('browser-sync').create();
        loadModuleTasks("watch");
        loadModuleTasks("styles");
        loadModuleTasks("handlebars");
        loadModuleTasks("scripts");
        loadModuleTasks("concat");
        loadModuleTasks("copy-assets");
        loadModuleTasks("revision");
        loadModuleTasks("deploy");
        loadModuleTasks("inject");
        loadModuleTasks("menu");
        loadModuleTasks("nsp");

        // --------------------------------------------------------------
        // Liste des taches principales
        // --------------------------------------------------------------
        gulp.task( "build:dev", gulp.series('setDEV', 'styles-all', 'scripts-all', 'deploy', 'inject') );
        gulp.task( "build:dev" ).description = "Construit une version de développement";
        
        gulp.task( "build:prod", gulp.series('setPROD', 'styles-all', 'scripts-all', 'deploy', 'inject') );
        gulp.task( "build:prod" ).description = "Construit une version de production";
    
        gulp.task( "serve:dev", gulp.series('setDEV', 'styles-all', 'scripts-all', 'deploy', 'inject', gulp.parallel('launchBrowserSync', 'watch') ) );
        gulp.task( "serve:dev" ).description = "Construit une version de developpement et lance un serveur de dev (watch + BrowserSync)";
        
        gulp.task( "default", gulp.series("menu") );
        
        // --------------------------------------------------------------
        // Gestion des erreurs
        // --------------------------------------------------------------
    
        var gulp_src = gulp.src;
        gulp.src = function() {
            if( $.environments.development() ){
                // Si on est en mode DEV
                // Stop proprement les taches en erreur SANS arrêter la sequence des autres taches
                // Cela evite d'avoir à relancer browserSync en cas d'erreur SASS !
                // De plus cela affiche les erreurs par l'intermédiaire des notifications de browserSync
                return gulp_src.apply(gulp, arguments)
                    .pipe($.plumber(function(error) {
                        global.browserSync.notify(
                            "<div style='width: 400px'>" +
                            "<div style='font-size: 4em; color: red;'>Erreur "+error.plugin+"</div>" +
                            "<div style='font-size: 1em; color: orange;'>"+error.message+"</div>" +
                            "</div>"
                        );
                        $.beeper();
                        // Output an error message
                        $.log.error('Error (' + error.plugin + '): ' + error.message);
                        // emit the end event, to properly end the task
                        this.emit('end');
                    })
                );
            } else  {
                // Si on est en mode PROD
                // Comportement par defaut :
                // En cas d'erreur, les autres tâches ne sont pas executées et Node revoie un code d'erreur 1
                return gulp_src.apply(gulp, arguments);
            }
        };
        
    }else{
        $.log("------------- PAS DE FICHIER DE CONFIGURATION : Lancement de la procédure d'installation -------------------");
        loadModuleTasks("install");
    }
    

};